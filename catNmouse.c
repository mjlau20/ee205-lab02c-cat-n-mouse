///////////////////////////////////////////////////////////////////////////////
//           University of Hawaii, College of Engineering
/// @brief   Lab02c - Cat `n Mouse - EE 205 - Spr 2022
//
/// This is a classic "I'm thinking of a number" guessing game.  The mouse
/// will think of a number... and the cat will keep trying to guess it.
///
/// @file    catNmouse.c
/// @version 1.0 - Initial version
///
/// Compile: $ gcc -o catNmouse catNmouse.c
///
/// Usage:  catNmouse [n]
///   n:  The maximum number used in the guessing game
///
/// Exit Status:
///   1:  The command line parameter was not a valid value
///   0:  The cat finally guessed correctly
///
/// Example:
///   $ ./catNmouse 2000
///   OK cat, I'm thinking of a number from 1 to 2000.  Make a guess:  1385
///   No cat... the number I’m thinking of is smaller than 1385
///   OK cat, I'm thinking of a number from 1 to 2000.  Make a guess:  1382
///   No cat... the number I’m thinking of is larger than 1382
///   OK cat, I'm thinking of a number from 1 to 2000.  Make a guess:  1384
///   You got me.
///   |\---/|
///   | o_o |
///    \_^_/
///   
/// @author  Michael Lau <mjlau20@hawaii.edu>
/// @date    27_01_2022
///////////////////////////////////////////////////////////////////////////////


#include <stdio.h>
#include <stdlib.h>

int main( int argc, char* argv[] ) {
   #define DEFAULT_MAX_NUMBER (2048)
   int theMaxValue;
   int aGuess;
   int theNumberImThinkingOf;

   if ( argv[1] == NULL ) {
      theMaxValue = DEFAULT_MAX_NUMBER; 
      theNumberImThinkingOf = ( rand() % theMaxValue ) + 1;

   } else { 
      theMaxValue = atoi( argv[1] );
      theNumberImThinkingOf = ( rand() % theMaxValue ) + 1;
   
   }

   do {

      printf( "OK cat, I’m thinking of a number from 1 to %d.  Make a guess: ", theMaxValue );
      scanf( "%d", &aGuess );

      if ( aGuess < 1 )  {
         printf( "You must enter a number that’s >= 1\n" );
         continue;
      }

      if ( aGuess > theMaxValue ) {
         printf( "You must enter a number that’s <= %d\n", theMaxValue );
         continue;
      }

      if ( aGuess > theNumberImThinkingOf ) {
         printf( "No cat... the number I’m thinking of is smaller than %d\n", aGuess );
         continue;
      }

      if ( aGuess < theNumberImThinkingOf ) {
         printf( "No cat... the number I’m thinking of is larger than %d\n", aGuess );
         continue;
      }

      if ( aGuess == theNumberImThinkingOf ) {
         printf( "You got me.\n" );
         break;
      }

   } while( aGuess != theNumberImThinkingOf );

   printf( "            .                . \n" );
   printf( "            :\"-.          .-\";\n" );
   printf( "            |:`.`.__..__.'.';|\n" );
   printf( "            || :-\"      \"-; ||\n" );
   printf( "            :;              :;\n" );
   printf( "            /  .==.    .==.  \\\n" );
   printf( "           :      _.--._      ;\n" );
   printf( "           ; .--.' `--' `.--. :\n" );
   printf( "          :   __;`      ':__   ;\n" );
   printf( "          ;  '  '-._:;_.-'  '  :\n" );
   printf( "          '.       `--'       .'\n" );
   printf( "           .\"-._          _.-\".\n" );
   printf( "         .'     \"\"------\"\"     `.\n" );
   printf( "        /`-                    -'\\\n" );
   printf( "       /`-                      -'\\\n" );
   printf( "      :`-   .'              `.   -';\n" );
   printf( "      ;    /                  \\    :\n" );
   printf( "     :    :                    ;    ;\n" );
   printf( "     ;    ;                    :    :\n" );
   printf( "     ':_:.'                    '.;_;'\n" );
   printf( "        :_                      _;\n" );
   printf( "        ; \"-._                -\" :`-.     _.._ \n" );
   printf( "        :_          ()          _;   \"--::__. `.\n" );
   printf( "         \\\"-                  -\"/`._           :\n" );
   printf( "        .-\"-.                 -\"-.  \"\"--..____.'\n" );
   printf( "       /         .__  __.         \\\n" );
   printf( "      : / ,       / \"\" \\       . \\ ;\n" );
   printf( "       \"-:___..--\"      \"--..___;-\"\n" );
   exit( 0 );

   return 1;
}

